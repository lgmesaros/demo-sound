import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';

import { Media, MediaObject } from '@ionic-native/media';
import { File } from '@ionic-native/file';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  filename: any = "Bach Gavotte Short";
  curr_playing_file: MediaObject;
  storageDirectory: any;

  is_playing: boolean = false;
  is_in_play: boolean = false;
  is_ready: boolean = false;

  message: any;

  duration: any = -1;
  duration_string: string;
  position: any = 0;

  get_duration_interval: any;
  get_position_interval: any;

  url: string = "http://www.hubharp.com/web_sound/BachGavotteShort.mp3";

  constructor(public navCtrl: NavController, public platform: Platform, public media: Media, public file: File) {
    
  }

  ionViewWillEnter() {
    this.platform.ready().then(() => {
      console.log('Platform ready');
      this.getDuration();
    });
  }

  playRecording() {
    this.curr_playing_file.play();
    this.getAndSetCurrentAudioPosition();
  }

  pausePlayRecording() {
    this.curr_playing_file.pause();
  }

  createAudioFile(): MediaObject {
    return this.media.create(this.url);
  }

  setToPlay() {
    var self = this;
    this.curr_playing_file = this.createAudioFile();
    this.curr_playing_file.onStatusUpdate.subscribe(status => {
      // 2: playing
      // 3: pause
      // 4: stop
      self.message = status;
      switch(status) {
        case 1:
          self.is_in_play = false;
          break;
        case 2:   // 2: playing
          self.is_in_play = true;
          self.is_playing = true;
          break;
        case 3:   // 3: pause
          self.is_in_play = true;
          self.is_playing = false;
          break;
        case 4:   // 4: stop
        default:
          self.is_in_play = false;
          self.is_playing = false;
          break;
      }
      console.log('Status: ', status);
    })
    console.log("audio file set");
    this.message = "audio file set";
    this.is_ready = true;
    this.curr_playing_file.onSuccess.subscribe(() => console.log('Action is successful'));
    this.curr_playing_file.onError.subscribe(error => console.log('Error: ', error));
  }

  getDuration() {
    this.curr_playing_file = this.createAudioFile();
    this.curr_playing_file.play();
    this.curr_playing_file.setVolume(0.0);
    let self = this;
    this.get_duration_interval = setInterval(function() {
      if(self.duration == -1) {
        self.duration = ~~(self.curr_playing_file.getDuration());
      } else {
        console.log('Duration: ', self.duration);
        self.curr_playing_file.stop();
        self.curr_playing_file.release();
        self.setToPlay();
        clearInterval(self.get_duration_interval);
      }
    }, 100);
  }

  getAndSetCurrentAudioPosition() {
    let diff = 1;
    let self = this;
    this.get_position_interval = setInterval(function() {
      let last_position = self.position;
      self.curr_playing_file.getCurrentPosition().then((position) => {
        if (position >= 0 && position < self.duration) {
          if(Math.abs(last_position - position) >= diff) {
            // set position
            self.curr_playing_file.seekTo(last_position*1000);
          } else {
            // update position for display
            self.position = position;
          }
        } else if (position >= self.duration) {
          self.stopPlayRecording();
        }
      });
    }, 100);
  }

  stopPlayRecording() {
    clearInterval(this.get_position_interval);
    this.curr_playing_file.stop();
    this.curr_playing_file.release();
    this.position = 0;
    this.setToPlay();
  }

}
